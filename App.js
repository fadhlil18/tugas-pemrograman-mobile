import React,{useState} from "react";
import { TouchableHighlight,Switch, Text, View, Image,SafeAreaView, StyleSheet, TextInput } from "react-native";

const UselessTextInput = () => { 
  const [text, onChangeText] = React.useState(null);
  const [number, onChangeNumber] = React.useState(null);
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);

  return (

    <SafeAreaView>
    <View style={styles.container}>
    <Text style={styles.title}>Mihoyo Present</Text>
    <Image
        source={{
          uri: 'https://cdn-2.tstatic.net/tribunnews/foto/bank/images/genshin-impact.jpg',
        }}
        style={{width:300,height:100,marginTop:1}}
    />
    </View>
      <TextInput
        style={styles.input}
        onChangeText={onChangeText}
        value={text}
        placeholder="Masukkan ID Genshin"
      />
      <TextInput
        style={styles.input}
        onChangeText={onChangeNumber}
        value={number}
        placeholder="Masukkan PIN Genshin"
        keyboardType="numeric"
      />
       <Image 
       style={styles.input}
        source={{
          uri: 'https://pbs.twimg.com/media/E67D6vWVEAMoe1i.png',
        }}
        style={{width:400,height:200,alignself:'center',marginTop:5}}
      />
      <Switch style={styles.container}
        trackColor={{ false: "#d9abab", true: "#00ffff" }}
        thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
        ios_backgroundColor="#3e3e3e"
        onValueChange={toggleSwitch}
        value={isEnabled}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  title: {
    paddingVertical: 1,
    borderWidth: 5,
    borderColor: "#20232a",
    borderRadius: 2,
    backgroundColor: "#808080",
    color: "#61dafb",
    textAlign: "center",
    fontSize: 30,
    fontWeight: "bold"
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10
  },
  container: {
    alignItems: "center",
    justifyContent: "center"
  },
});

export default UselessTextInput;
